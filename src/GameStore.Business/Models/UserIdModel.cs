﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameStore.Business.Models
{
    public class UserIdModel
    {
        public Guid Id { get; set; }
    }
}
