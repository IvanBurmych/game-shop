﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameStore.Business.Models
{
    public class GameGenreModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int GameId { get; set; }
    }
}
