﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameStore.Business.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OrderType { get; set; }
        public bool IsConfirmed { get; set; }
        public ICollection<int> GameId { get; set; }
    }
}
