﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameStore.Business.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public Guid AuthorId { get; set; }
        public int GameId { get; set; }
        public ICollection<int> ChildCommentsIds { get; set; }
    }
}
