﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GameStore.Business.Models
{
    public class GameModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Data{ get; set; }
        public double Cost { get; set; }
        public ICollection<int> GenresIds { get; set; }
        public ICollection<int> CommentsIds { get; set; }
        public Guid AuthorId { get; set; }
    }
}
