﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameStore.Business.Models
{
    public class UserLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
