﻿using AutoMapper;
using GameStore.Business.Interfaces;
using GameStore.Business.Models;
using GameStore.Data.Interfaces;
using GameStore.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GameStore.Data.Repositories;

namespace GameStore.Business.Services
{
    public class CommentService : ICommentService
    {
        private readonly CommentRepository repository;
        private readonly IMapper mapper;
        public CommentService(CommentRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public async Task AddCommentAsync(CommentModel model)
        {
            var comment = mapper.Map<Comment>(model);
            await repository.AddAsync(comment);
        }
        public async Task DeleteCommentByIdAsync(int id)
        {
            await repository.DeleteByIdAsync(id);
        }
        public async Task<IEnumerable<CommentModel>> GetAllComment()
        {
            var comments = await repository.GetAllWithDetails();
            List<CommentModel> models = new List<CommentModel>();

            foreach (var comment in comments)
            {
                models.Add(mapper.Map<CommentModel>(comment));
            }
            return models;
        }
        public async Task<CommentModel> GetCommentByIdAsync(int id)
        {
            var comment = await repository.GetByIdWithDetails(id);
            var model = mapper.Map<CommentModel>(comment);
            return model;
        }
        public async void UpdateCommentAsync(CommentModel model)
        {
            var comment = mapper.Map<Comment>(model);
            await repository.Update(comment);
        }
    }
}
