﻿using AutoMapper;
using GameStore.Business.Interfaces;
using GameStore.Business.Models;
using GameStore.Data.Entities;
using GameStore.Data.Interfaces;
using GameStore.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Business.Services
{
    public class GameGenreService : IGameGenreService
    {
        private readonly GameGenreRepository repository;
        private readonly IMapper mapper;
        public GameGenreService(GameGenreRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public async Task AddGenreAsync(GameGenreModel model)
        {
            var genre = mapper.Map<GameGenre>(model);
            await repository.AddAsync(genre);
        }
        public async Task DeleteGenreByIdAsync(int id)
        {
            await repository.DeleteByIdAsync(id);
        }
        public async Task<IEnumerable<GameGenreModel>> GetAllGenres()
        {
            var genres = await repository.GetAllWithDetails();
            List<GameGenreModel> models = new List<GameGenreModel>();

            foreach (var genre in genres)
            {
                models.Add(mapper.Map<GameGenreModel>(genre));
            }
            return models;
        }
        public async Task<GameGenreModel> GetGenreByIdAsync(int id)
        {
            var genre = await repository.GetByIdWithDetails(id);
            return mapper.Map<GameGenreModel>(genre);
        }
        public void UpdateGenreAsync(GameGenreModel model)
        {
            var rating = mapper.Map<GameGenre>(model);
            repository.Update(rating);
        }
    }
}
