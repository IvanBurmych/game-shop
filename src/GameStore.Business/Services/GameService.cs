﻿using AutoMapper;
using GameStore.Business.Interfaces;
using GameStore.Business.Models;
using GameStore.Data.Entities;
using GameStore.Data.Interfaces;
using GameStore.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Business.Services
{
    public class GameService : IGameService
    {
        private readonly GameRepository repository;
        private readonly IMapper mapper;
        public GameService(GameRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public async Task AddGameAsync(GameModel model)
        {
            var game = mapper.Map<Game>(model);
            await repository.AddAsync(game);
        }
        public async Task DeleteGameByIdAsync(int id)
        {
            await repository.DeleteByIdAsync(id);
        }
        public async Task<IEnumerable<GameModel>> GetAllGames()
        {
            var games = await repository.GetAllWithDetails();
            List<GameModel> models = new List<GameModel>();

            foreach (var game in games)
            {
                models.Add(mapper.Map<GameModel>(game));
            }
            return models;
        }
        public async Task<IEnumerable<CommentModel>> GetComentsByGameId(int id)
        {
            var game = await repository.GetByIdWithDetails(id);
            var comments = game.Comments;
            List<CommentModel> models = new List<CommentModel>();

            foreach (var comment in comments)
            {
                models.Add(mapper.Map<CommentModel>(comments));
            }
            return models;
        }
        public async Task<GameModel> GetGameByIdAsync(int id)
        {
            var game = await repository.GetByIdWithDetails(id);
            return mapper.Map<GameModel>(game);
        }
        public async Task<IEnumerable<GameModel>> GetGamesByAuthorId(Guid id)
        {
            var games = await repository.GetGamesByAuthorId(id);
            List<GameModel> models = new List<GameModel>();

            foreach (var game in games)
            {
                models.Add(mapper.Map<GameModel>(game));
            }
            return models;
        }
        public async Task<IEnumerable<GameModel>> GetGamesByAuthorName(string authorName)
        {
            var games = await repository.GetGamesByAuthorName(authorName);
            List<GameModel> models = new List<GameModel>();

            foreach (var game in games)
            {
                models.Add(mapper.Map<GameModel>(game));
            }
            return models;
        }
        public async Task<IEnumerable<GameModel>> GetGamesByGenreName(string gameGenreName)
        {
            var games = await repository.GetGamesByGenreName(gameGenreName);
            List<GameModel> models = new List<GameModel>();

            foreach (var game in games)
            {
                models.Add(mapper.Map<GameModel>(game));
            }
            return models;
        }
        public async Task<IEnumerable<GameModel>> GetGamesByWord(string word)
        {
            var games = await repository.GetGamesByWord(word);
            List<GameModel> models = new List<GameModel>();

            foreach (var game in games)
            {
                models.Add(mapper.Map<GameModel>(game));
            }
            return models;
        }
        public async Task<IEnumerable<GameModel>> GetGamesByYear(int year)
        {
            var games = await repository.GetGamesByYear(year);
            List<GameModel> models = new List<GameModel>();

            foreach (var game in games)
            {
                models.Add(mapper.Map<GameModel>(game));
            }
            return models;
        }
        public async Task<IEnumerable<GameGenreModel>> GetGenresByGameId(int id)
        {
            var game = await repository.GetByIdWithDetails(id);
            var genres = game.Genres;
            List<GameGenreModel> models = new List<GameGenreModel>();

            foreach (var genre in genres)
            {
                models.Add(mapper.Map<GameGenreModel>(genre));
            }
            return models;
        }
        public async Task UpdateGameAsync(GameModel model)
        {
            var game = mapper.Map<Game>(model);
            await repository.Update(game);
        }
    }
}
