﻿using GameStore.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Business.Interfaces
{
    public interface IGameService
    {
        Task<IEnumerable<GameModel>> GetAllGames();
        Task<GameModel> GetGameByIdAsync(int id);
        Task<IEnumerable<GameModel>> GetGamesByGenreName(string gameGenreName);
        Task<IEnumerable<GameModel>> GetGamesByWord(string word);
        Task<IEnumerable<GameModel>> GetGamesByAuthorId(Guid id);
        Task<IEnumerable<GameModel>> GetGamesByYear(int year);
        Task AddGameAsync(GameModel model);
        Task UpdateGameAsync(GameModel model);
        Task DeleteGameByIdAsync(int id);

        Task<IEnumerable<CommentModel>> GetComentsByGameId(int id);
        Task<IEnumerable<GameGenreModel>> GetGenresByGameId(int id);
        Task<IEnumerable<GameModel>> GetGamesByAuthorName(string authorName);
    }
}
