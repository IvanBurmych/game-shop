﻿using GameStore.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Business.Interfaces
{
    public interface IGameGenreService
    {
        Task<IEnumerable<GameGenreModel>> GetAllGenres();
        Task<GameGenreModel> GetGenreByIdAsync(int id);
        Task AddGenreAsync(GameGenreModel model);
        void UpdateGenreAsync(GameGenreModel model);
        Task DeleteGenreByIdAsync(int id);
    }
}
