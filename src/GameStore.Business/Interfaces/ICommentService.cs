﻿using GameStore.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Business.Interfaces
{
    public interface ICommentService
    {
        Task<IEnumerable<CommentModel>> GetAllComment();
        Task<CommentModel> GetCommentByIdAsync(int id);
        Task AddCommentAsync(CommentModel model);
        void UpdateCommentAsync(CommentModel model);
        Task DeleteCommentByIdAsync(int id);
    }
}
