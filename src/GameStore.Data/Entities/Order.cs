﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameStore.Data.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OrderType { get; set; }
        public bool IsConfirmed { get; set; }
        public Game Game { get; set; }
    }
}
