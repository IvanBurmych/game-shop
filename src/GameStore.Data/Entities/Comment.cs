﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GameStore.Data.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        [MaxLength(600)]
        public string Content { get; set; }
        public Guid AuthorId { get; set; }
        public Game Game { get; set; }
        public ICollection<Comment> ChildComments { get; set; }
    }
}
