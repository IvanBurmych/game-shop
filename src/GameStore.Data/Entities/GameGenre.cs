﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameStore.Data.Entities
{
    public class GameGenre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Game Game { get; set; }
    }
}
