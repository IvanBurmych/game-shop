﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace GameStore.Data.Entities
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Data { get; set; }
        public double Cost { get; set; }
        public ICollection<GameGenre> Genres { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public Guid AuthorId { get; set; }
    }
}
