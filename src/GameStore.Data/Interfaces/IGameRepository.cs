﻿using GameStore.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Data.Interfaces
{
    public interface IGameRepository : IRepository<Game>
    {
        Task<IEnumerable<Game>> GetGamesByAuthorId(Guid id);
        Task<IEnumerable<Game>> GetGamesByGenreName(string genreName);
        Task<IEnumerable<Game>> GetGamesByWord(string word);
        Task<IEnumerable<Game>> GetGamesByYear(int year);
        Task<IEnumerable<Game>> GetAllWithDetails();
        Task<Game> GetByIdWithDetails(int id);
        Task<IEnumerable<Game>> GetGamesByAuthorName(string authorName);
    }
}
