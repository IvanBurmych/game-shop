﻿using GameStore.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Data.Interfaces
{
    public interface IGameGenreRepository : IRepository<GameGenre>
    {
        Task<IEnumerable<GameGenre>> GetAllWithDetails();
        Task<GameGenre> GetByIdWithDetails(int id);
    }
}
