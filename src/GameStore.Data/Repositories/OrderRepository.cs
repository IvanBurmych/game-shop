﻿using Data;
using GameStore.Data.Entities;
using GameStore.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Data.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(DataContext context) : base(context)
        {
        }
        public async Task<IEnumerable<Order>> GetAllWithDetails()
        {
            return await Context.Orders.Include(x => x.Game).ToListAsync();
        }
        public Task<Order> GetByIdWithDetails(int id)
        {
            return Context.Orders.Where(x => x.Id == id).Include(x => x.Game).FirstOrDefaultAsync();
        }
    }
}
