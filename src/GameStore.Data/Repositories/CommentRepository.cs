﻿using Data;
using GameStore.Data.Entities;
using GameStore.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Data.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(DataContext context) : base(context)
        {
        }
        public async Task<IEnumerable<Comment>> GetAllWithDetails()
        {
            return await Context.Comments.Include(x => x.Game).ToListAsync();
        }
        public async Task<Comment> GetByIdWithDetails(int id)
        {
            return await Context.Comments.Where(x => x.Id == id).Include(x => x.Game).FirstOrDefaultAsync();
        }

    }
}
