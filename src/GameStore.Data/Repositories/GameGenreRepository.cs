﻿using Data;
using GameStore.Data.Entities;
using GameStore.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Data.Repositories
{
    public class GameGenreRepository : Repository<GameGenre>, IGameGenreRepository
    {
        public GameGenreRepository(DataContext context) : base(context)
        {
        }
        public async Task<IEnumerable<GameGenre>> GetAllWithDetails()
        {
            return await Context.GameGenres.Include(x => x.Game).ToListAsync();
        }
        public Task<GameGenre> GetByIdWithDetails(int id)
        {
            return Context.GameGenres.Where(x => x.Id == id).Include(x => x.Game).FirstOrDefaultAsync();
        }
    }
}
