﻿using Data;
using GameStore.Data.Entities;
using GameStore.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Data.Repositories
{
    public class GameRepository : Repository<Game>, IGameRepository
    {
        public GameRepository(DataContext context) : base(context)
        {
        }
        public async Task<IEnumerable<Game>> GetAllWithDetails()
        {
            return await Context.Games
                .Include(x => x.Comments)
                .Include(x => x.Genres)
                .ToListAsync();
        }
        public async Task<Game> GetByIdWithDetails(int id)
        {
            return await Context.Games
                .Where(x => x.Id == id)
                .Include(x => x.Comments)
                .Include(x => x.Genres)
                .FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<Game>> GetGamesByAuthorId(Guid authorId)
        {
            return await Context.Games
                .Where(x => x.AuthorId == authorId)
                .Include(x => x.Comments)
                .Include(x => x.Genres)
                .ToListAsync();
        }
        public async Task<IEnumerable<Game>> GetGamesByGenreName(string genreName)
        {
            return await Context.GameGenres
                .Where(x => x.Name == genreName)
                .Select(x => x.Game)
                .Include(x => x.Comments)
                .Include(x => x.Genres)
                .ToListAsync();
        }
        public async Task<IEnumerable<Game>> GetGamesByWord(string word)
        {
            return await Context.Games
                .Where(x => x.Name.Contains(word))
                .Include(x => x.Comments)
                .Include(x => x.Genres)
                .ToListAsync();
        }
        public async Task<IEnumerable<Game>> GetGamesByYear(int year)
        {
            return await Context.Games
                .Where(x => x.Data.Year == year)
                .Include(x => x.Comments)
                .Include(x => x.Genres)
                .ToListAsync();
        }
        public async Task<IEnumerable<Game>> GetGamesByAuthorName(string authorName)
        {
            var usersIds = Context.Users
                .Where
                (
                    x => x.LastName.Contains(authorName) || 
                    x.FirstName.Contains(authorName) ||
                    (x.FirstName + x.LastName).Contains(authorName)
                )
                .Select(x => x.Id);

            return await Context.Games
                .Where(x => usersIds.Contains(x.AuthorId))
                .Include(x => x.Comments)
                .Include(x => x.Genres)
                .ToListAsync();
        }
    }
}
