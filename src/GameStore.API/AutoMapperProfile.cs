﻿using AutoMapper;
using GameStore.Business.Models;
using GameStore.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.API
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserRegisterModel, AppUser>();


            CreateMap<Comment, CommentModel>()
                .ForMember(u => u.GameId, opt => opt.MapFrom(ur => ur.Game.Id))
                .ForMember(u => u.ChildCommentsIds, opt => opt.MapFrom(ur => ur.ChildComments.Select(x => x.Id)))
                .ReverseMap();

            CreateMap<Game, GameModel>()
                .ForMember(u => u.CommentsIds, opt => opt.MapFrom(ur => ur.Comments.Select(x => x.Id)))
                .ForMember(u => u.GenresIds, opt => opt.MapFrom(ur => ur.Genres.Select(x => x.Id)))
                .ReverseMap();

            CreateMap<GameGenre, GameGenreModel>()
                .ForMember(u => u.GameId, opt => opt.MapFrom(ur => ur.Game.Id))
                .ReverseMap();

            CreateMap<Order, OrderModel>()
                .ForMember(u => u.GameId, opt => opt.MapFrom(ur => ur.Game.Id))
                .ReverseMap();
        }
    }
}
