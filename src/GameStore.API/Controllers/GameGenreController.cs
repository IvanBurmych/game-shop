﻿using GameStore.Business.Interfaces;
using GameStore.Business.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GameStore.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameGenreController : Controller
    {
        private readonly IGameGenreService service;
        public GameGenreController(IGameGenreService service)
        {
            this.service = service;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> AddGenre(GameGenreModel model)
        {
            await service.AddGenreAsync(model);
            return Ok();
        }
        [HttpGet]
        public async Task<ActionResult<GameGenreModel>> GetAllGenres()
        {
            return Ok(await service.GetAllGenres());
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<GameGenreModel>> GetAllGenres(int id)
        {
            return Ok(await service.GetGenreByIdAsync(id));
        }
    }
}
