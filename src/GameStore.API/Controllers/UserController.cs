﻿using AutoMapper;
using GameStore.Business.Models;
using GameStore.Data.Entities;
using GameStore.API.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        public UserController(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("{userName}/Role/{roleName}")]
        public async Task<IActionResult> AddUserToRole(string userName, string roleName)
        {
            var user = userManager.Users.SingleOrDefault(u => u.UserName == userName);

            var result = await userManager.AddToRoleAsync(user, roleName);

            if (result.Succeeded)
            {
                return Ok();
            }
            return Problem(result.Errors.First().Description, null, 500);
        }
    }
}
