﻿using GameStore.API.ExceptionHandling.Exceptions;
using GameStore.Business.Interfaces;
using GameStore.Business.Models;
using GameStore.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : Controller
    {
        private readonly IGameService service;
        private readonly UserManager<AppUser> userManager;

        public GameController(IGameService service, UserManager<AppUser> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<GameModel>>> GetAllGames()
        {
            return Ok(await service.GetAllGames());
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<GameModel>> GetGameById(int id)
        {
            return Ok(await service.GetGameByIdAsync(id));
        }
        [HttpGet("genre/{genre}")]
        public async Task<ActionResult<GameModel>> GetGamesByGenre(string genre)
        {
            return Ok(await service.GetGamesByGenreName(genre));
        }
        [HttpGet("authorId/{UserId}")]
        public async Task<ActionResult<GameModel>> GetGamesByAuthorId(Guid UserId)
        {
            return Ok(await service.GetGamesByAuthorId(UserId));
        }
        [HttpGet("authorName/{authorName}")]
        public async Task<ActionResult<IEnumerable<GameModel>>> GetGamesByAuthorName(string authorName)
        {
            return Ok(await service.GetGamesByAuthorName(authorName));
        }
        [Authorize]
        [HttpPost("add")]
        public async Task<IActionResult> AddGame(GameModel model)
        {
            var authorId = userManager.FindByNameAsync(HttpContext.User.Identity.Name).Result.Id;
            model.AuthorId = authorId;
            model.Data = DateTime.Now;
            await service.AddGameAsync(model);

            return Ok();
        }
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGame(int id)
        {
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var userRoles = await userManager.GetRolesAsync(user);

            var game = await service.GetGameByIdAsync(id);

            if (userRoles.Contains("Admin") || userRoles.Contains("Manager") || game.AuthorId == user.Id)
            {
                await service.DeleteGameByIdAsync(id);
                return Ok();
            }
            else
            {
                return BadRequest("Not permitted");
            }
        }
        [Authorize]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateGame(GameModel model)
        {
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var userRoles = await userManager.GetRolesAsync(user);

            var game = await service.GetGameByIdAsync(model.Id);

            if (userRoles.Contains("Admin") || userRoles.Contains("Manager") || game.AuthorId == user.Id)
            {
                await service.UpdateGameAsync(model);
                return Ok();
            }
            else
            {
                return BadRequest("Not permitted");
            }
        }

    }
}
