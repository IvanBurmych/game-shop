﻿using Microsoft.AspNetCore.Builder;
using System.Linq;

namespace GameStore.API.ExceptionHandling
{
    public static class DependecyLoader
    {
        public static void Use(IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
