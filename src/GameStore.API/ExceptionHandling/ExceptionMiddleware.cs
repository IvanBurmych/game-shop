﻿using GameStore.API.ExceptionHandling.Exceptions;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace GameStore.API.ExceptionHandling
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (RootHandlingException exception)
            {
                context.Response.StatusCode = (int)exception.StatusCode;
                context.Response.ContentType = "application/json";
                var jsonObj = JsonSerializer.Serialize(new
                {
                    errorMessage = exception.Message
                });
                await context.Response.WriteAsync(jsonObj);
            }
            catch
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Response.ContentType = "application/json";
            }
        }
    }
}
