﻿using System.Net;

namespace GameStore.API.ExceptionHandling.Exceptions
{
    public class NotFoundException : RootHandlingException
    {
        public NotFoundException()
        : base(HttpStatusCode.NotFound, "Not found")
        {
        }

        public NotFoundException(string message)
        : base(HttpStatusCode.NotFound, message)
        {
        }
    }
}
