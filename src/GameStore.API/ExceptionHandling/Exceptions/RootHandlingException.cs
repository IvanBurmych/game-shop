﻿using System;
using System.Net;

namespace GameStore.API.ExceptionHandling.Exceptions
{
    public class RootHandlingException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        public RootHandlingException(HttpStatusCode statusCode) : base()
        {
            StatusCode = statusCode;
        }

        public RootHandlingException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public RootHandlingException(HttpStatusCode statusCode, string message, Exception innerException) : base(message, innerException)
        {
            StatusCode = statusCode;
        }
    }
}
