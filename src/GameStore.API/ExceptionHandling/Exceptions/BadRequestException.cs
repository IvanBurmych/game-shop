﻿using System.Net;

namespace GameStore.API.ExceptionHandling.Exceptions
{
    public class BadRequestException : RootHandlingException
    {
        public BadRequestException()
        : base(HttpStatusCode.BadRequest, "Bad request")
        {
        }

        public BadRequestException(string message)
        : base(HttpStatusCode.BadRequest, message)
        {
        }
    }
}
