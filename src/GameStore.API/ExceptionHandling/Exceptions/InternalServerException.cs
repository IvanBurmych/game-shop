﻿using System;
using System.Net;

namespace GameStore.API.ExceptionHandling.Exceptions
{
    public class InternalServerException : RootHandlingException
    {
        public InternalServerException()
        : base(HttpStatusCode.NotFound, "Not found")
        {
        }

        public InternalServerException(string message)
        : base(HttpStatusCode.NotFound, message)
        {
        }
    }
}
